<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmailsTest extends WebTestCase
{
    public function testSomething()
    {
        $client = static::createClient();
        $client->enableProfiler();
        $crawler = $client->request('POST', '/profile/car/export/');

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        //dump($mailCollector);
        $this->assertSame(1,$mailCollector->getMessageCount());

        $collectMessages = $mailCollector->getMessages();
        $message = $collectMessages[0];

        $this->assertInstanceOf('Swift_Message', $message);
        $this->assertSame('Your car list', $message->getSubject());
        $this->assertSame('romeokamgo@gmail.com', key($message->getFrom()));
        $this->assertSame('romeokamgo@gmail.com', key($message->getTo()));
        $this->assertContains(' Thank you for visiting our shop!', $message->getBody());
    }
}

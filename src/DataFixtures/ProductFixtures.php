<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Product;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_on`, `lastlogin`) VALUES (NULL, 'test', 'test@vcat.de', 'test', '2020-02-11 16:58:13', '2020-02-11 16:58:13');
        foreach ($this->getProductData() as [$name, $description, $quantity, $price, $available, $images])
        {
            $product = new Product();
            //$product->setCategory($category);
            $product->setName($name);
            $product->setDescription($description);
            $product->setQuantity($quantity);
            $product->setPrice($price);
            $product->setAvailable($available);
            $product->setImages($images);
            $manager->persist($product);
        }

        $manager->flush();
    }
    // to create fak data
    private function getProductData()
    {
        return[
            ['Prodtest1', 'Prd desc 1',10, 90.5, 1,'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest2', 'Prd desc 2',5, 100.0, 1, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest3', 'Prd desc 3',8, 85.5, 0, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest4', 'Prd desc 2',9, 85.5, 1, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest5', 'Prd desc 3',8, 52.5, 0, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest6', 'Prd desc 1',7, 85.5, 1, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest7', 'Prd desc 3',8, 10.5, 0, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
            ['Prodtest8', 'Prd desc 3',8, 85.5, 1, 'https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg'],
        ];
    }
}

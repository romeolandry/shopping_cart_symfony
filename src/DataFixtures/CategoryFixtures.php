<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_on`, `lastlogin`) VALUES (NULL, 'test', 'test@vcat.de', 'test', '2020-02-11 16:58:13', '2020-02-11 16:58:13');
        foreach ($this->getCategoryData() as [$name, $description, $available])
        {
            $category = new Category();
            $category->setName($name);
            $category->setDescription($description);
            $category->setAvailable($available);
            $manager->persist($category);
        }

        $manager->flush();
    }
    // to create fak data
    private function getCategoryData()
    {
        return[
            ['test Category1','Supper', 1],
            ['test Category2','Supper', 1],
            ['test Category3','Supper', 0]
        ];
    }
}

<?php

namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $password_encoder)
    {
        $this->password_encoder = $password_encoder;
    }
    public function convert_string_to_date($stringdate):\DateTime
    {
        return new \DateTime($stringdate);
    }


    public function load(ObjectManager $manager)
    {
        //INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_on`, `lastlogin`) VALUES (NULL, 'test', 'test@vcat.de', 'test', '2020-02-11 16:58:13', '2020-02-11 16:58:13');
        foreach ($this->getUserData() as [$name, $email, $password, $roles,$create_on,$lastlogin])
        {
            $user = new User();
            $user->setName($name);
            $user->setEmail($email);
            $user->setPassword($this->password_encoder->encodePassword($user, $password));
            $user->setRoles($roles);
            $user->setCreateOn($this->convert_string_to_date($create_on));
            $user->setLastlogin($this->convert_string_to_date($lastlogin));
            $manager->persist($user);
        }

        $manager->flush();
    }
    // to create fak data
    private function getUserData()
    {
        return[
            ['Kamgo Romeo','romeokamgo@vcat.de','romeo',['ROLE_ADMIN'], '2020-02-01 16:58:13', '2020-02-11 16:58:13'],
            ['test','test@vcat.de','test',['ROLE_USER'], '2020-02-11 16:58:13', '2020-02-08 16:58:13'],
            ['landry','landry@vcat.de','landry',['ROLE_USER'], '2020-02-11 16:58:13', '2020-02-05 16:58:13']
        ];
    }
}

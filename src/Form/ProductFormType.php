<?php

namespace App\Form;


use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFormType extends AbstractType
{
    public $lastImages;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'=>'Name',
            ])
            ->add('description', TextareaType::class,[
                'label'=>'give description',
            ])
            ->add('quantity', IntegerType::class, [
                'label'=>'Quantity'
            ])
            ->add('price', MoneyType::class, [
                'label'=>'Price'
            ])
            //->add('created')
            ->add('available', ChoiceType::class, [
                'label'=>'Available',
                'choices'=>[
                    'true'=>1,
                    'false'=>0
                ],
                'required'=>false
            ])

            ->add('category', EntityType::class, [
                'class' =>Category::class,
                'choice_label'=>'name'
            ])
             ->add('images', FileType::class, [
                 'label'=>'Image (jpd/png)',
                 'mapped' =>false,
                 'required'=>false
             ]);
        ;

        /* add event for update PRE_SET_DATA to construct form
        If user wont to do update just load form without file input*/
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function
        (FormEvent $events){
            # get Product Entity
            $product = $events->getData();
            $form = $events->getForm();

            if (!$product || null === $product->getId())
            {
               $form->add('save', SubmitType::class,[
                    'label'=>'Save Product',
                    'attr'=>[
                        'class'=>'btn btn-success'
                    ]
                ]);
            }
            else
            {
                $this->lastImages= $product->getImages();
                $form->add('update', SubmitType::class,[
                    'label'=>'Update',
                    'attr'=>[
                        'class'=>'btn btn-success'
                    ]
                ]);
            }

        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

}

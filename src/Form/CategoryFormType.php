<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'=>'Name',
                'data' => 'name'
            ])
            ->add('description', TextareaType::class,[
                'label'=>'give description',
                'data' => 'Description'
            ])
            ->add('available', ChoiceType::class, [
                'label'=>'Available',
                'choices'=>[
                    'true'=>1,
                    'false'=>0
                ],
                'required'=>false
            ])
            ->add('save', SubmitType::class,[
                'label'=>"Save Product"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}

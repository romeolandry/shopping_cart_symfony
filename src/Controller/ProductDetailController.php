<?php

namespace App\Controller;

use App\Entity\Product;
use App\Services\ServicesUsersCarCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Environment;

/**
 * @Route("/profile")
 */

class ProductDetailController extends AbstractController
{
    /**
     * @var Environment
     * @param ServicesUsersCarCommand $servicesUsersCarCommand
     * @param UserInterface $user
     */
    private $twig;
    private $car_value;


    public function __construct(Environment $twig,  ServicesUsersCarCommand $servicesUsersCarCommand, Security $security)
    {
        $this->twig = $twig;
        // get all open car for the connected user
        $user = $security->getUser();
        $this->car_value = $servicesUsersCarCommand->countOpenUserCar($user->getId());
    }

    /**
     * @Route("/product/detail/{id}", name="product_detail", methods={"GET"}, requirements={"id":"\d+"})
     * @param Request $request
     * @param Product $product
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public  function marketplace_more (Request $request, Product $product)
    {
        $quantity = 1;
        // get the selected Product from DB
        $product = $this->getDoctrine()->getRepository(Product::class)->find($request->attributes->get("id"));
        if (!$product instanceof Product) {
            return;
        }
        return new Response($this->twig->render('product_detail/index.html.twig',
            ['product'=>$product,
                'quantity'=>$quantity,
                'car_value'=> $this->car_value]));
    }
}

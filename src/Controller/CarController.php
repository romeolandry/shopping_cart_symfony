<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Product;
use App\Services\ServicesUsersCarCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Environment;

/**
 * @Route("/profile")
 */

class CarController extends AbstractController
{
    private $openCar;
    private $car_value;
    private $bil;

    public function __construct(Environment $twig,  ServicesUsersCarCommand $servicesUsersCarCommand, Security $security)
    {
        $this->twig = $twig;
        // get all open car for the connected user
        $user = $security->getUser();
        $this->car_value = $servicesUsersCarCommand->countOpenUserCar($user->getId());
        $this->openCar = $servicesUsersCarCommand->getNotCommandedCar($user->getId());
        $this->bil = $servicesUsersCarCommand->bil($user->getId());
    }

    /**
     * @Route("/car", name="car")
     * @return Response
     */
    public function index()
    {
        if(count($this->openCar)==0)
        {
            return $this->redirectToRoute('marketplace');
        }

        if ($this->isGranted('CAR_VIEW',$this->openCar[0]))
        {
            #ump("redirect to login");
           $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        }

        return $this->render('car/index.html.twig', [
            'open_car' => $this->openCar,
            'car_value' => $this->car_value,
            'bil' => $this->bil
        ]);
    }

    /**
     * @Route("/addCart/{id}/", name="add_product_to_cart", methods={"GET"})
     * @param Request $request
     * @param Product $product
     * @param UserInterface $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public  function add_to_cart (Request $request, Product $product, UserInterface $user)
    {
        //check if the product exist in a opened car
        $car_id = null;
        $make_it =0;
        $entityManager = $this->getDoctrine()->getManager();
        $quantity = $request->query->get('quantity');

        // check if product quantity
        if($quantity <= $product->getQuantity())
        {
            foreach ($this->openCar as $item)
            {
                if($item->getProduct()->getId() == $product->getId())
                {
                    $car_id = $item->getId();
                }
            }
            if ($car_id == null)
            {
                // insert in to cart
                $car = new Car();
                $car->setUser($user);
                $car ->setProduct($product);
                $car->setQuantity($quantity);
                $car->getProduct()->setQuantity($product->getQuantity()-$quantity);
                if($car->getProduct()->getQuantity()==0)
                {
                    $car->getProduct()->setAvailable(0);
                }
                dump($car->getProduct()->getAvailable());
                // save new user in to DB
                $entityManager->persist($car);
                $entityManager->persist($car->getProduct());
                //$entityManager->persist($product);
                $entityManager->flush();
            }
            else
            {
                // update car
                $car = $entityManager->getRepository(Car::class)->find($car_id);
                $car->setQuantity($car->getQuantity() + $quantity);
                $car->setPrice($car->getproduct()->getPrice() * $car->getQuantity());
                $car->getProduct()->setQuantity($product->getQuantity()-$quantity);
                {
                    $car->getProduct()->setAvailable(0);
                }
                // save new user in to DB
                $entityManager->persist($car);
                $entityManager->persist($car->getProduct());
                //$entityManager->persist($product);
                $entityManager->flush();
            }
        }
        else
        {
            // zu große
            dump("it not possible");
        }

        return $this->redirectToRoute('marketplace');
    }


    /**
     * @Route("/cart_add_one/{id}/{quantity}", name="update_car_add_one", methods={"GET"})
     * @param Request $request
     * @param Car $car
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update_quantity_add_one (Request $request, Car $car)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $car->setQuantity($car->getQuantity()+1);
        $car->setPrice($car->getproduct()->getPrice() * $car->getQuantity());
        $entityManager->persist($car);
        $entityManager->flush();

        return $this->redirectToRoute('car');
    }

    /**
     * @Route("/cart_remove_one/{id}/{quantity}", name="update_car_remove_one", methods={"GET"}, requirements={"quantity":"\d+"})
     * @param Request $request
     * @param Car $car
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update_quantity_remove_one (Request $request, Car $car)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $car->setQuantity($car->getQuantity()-1);
        if ($car->getQuantity()==0) {
            // remove product
            $entityManager->remove($car);
            $entityManager->flush();
        }
        else
        {
            $car->setPrice($car->getproduct()->getPrice() * $car->getQuantity());
            $entityManager->persist($car);
            $entityManager->flush();

        }
        return $this->redirectToRoute('car');

    }

    /**
     * @Route("/cart_remove/{id}", name="remove_Product_car", methods={"GET"}, requirements={"id":"\d+"})
     * @param Request $request
     * @param Car $car
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove_product_from_car (Request $request, Car $car)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $quantity_car = $car->getQuantity();
        $quantity_product = $car->getProduct()->getQuantity();

        if($car->getProduct()->getAvailable() == 1)
        {
            $car->getProduct()->setQuantity($quantity_car + $quantity_product);
        }
        else
        {
            $car->getProduct()->setQuantity($quantity_car);
            $car->getProduct()->setAvailable( 1);
        }
        // persist product
        $entityManager->persist($car->getProduct());
        // remove car
        $entityManager->remove($car);
        $entityManager->flush();

        return $this->redirectToRoute('car');
    }

    /**
     * @Route("/car/export/", name="car_export")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function export_car(Request $request, \Swift_Mailer $mailer){

        // convert  all opened car to array
        $array_of_car = array(['Product','Quantity','Price']);
        $total_amount = 0;
        foreach ($this->openCar as $car)
        {

            $flied = [
                "Product"=>$car->getProduct()->getName(),
                "Quantity"=>$car->getQuantity(),
                "Price"=>$car->getProduct()->getPrice()* $car->getQuantity()

            ];
            $total_amount =  $total_amount + $car->getProduct()->getPrice()* $car->getQuantity();
            array_push($array_of_car,$flied);
        }
        $flied = ['','Total to pay:', $total_amount];
        array_push($array_of_car,$flied);
        // write ro file
        $fp = fopen('car.csv','w+');
        foreach ($array_of_car as $item)
        {
            fputcsv($fp, $item);
        }
        fclose($fp);

        $message = (new \ Swift_Message('Your car list'))
            ->setFrom('mailsend@mailtest.vcat.de')
            ->setTo($this->getUser()->getUsername())
            ->setBody(
                $this->renderView(
                    'email/EmailContentExportCar.html.twig'
                ),
                'text/html'
            )
        ->attach(\Swift_Attachment::fromPath('car.csv'))
        ;
        $mailer->send($message);
        $this->addFlash(
            'info',
            "You received an Email!"
        );

        return $this->redirectToRoute('car');

    }
}

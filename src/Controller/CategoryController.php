<?php


namespace App\Controller;


use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends  AbstractController
{
    /**
     *@Route("/categorie", name="cat")
    */
    public function persistTest()
    {
        $entity = $this->getDoctrine()->getManager();
        $cat = new Category();
        $cat->setName("test");
        $cat->setDescription("desc");
        $entity->persist($cat);
        $entity->flush();

        return $this->render('homepage.html.twig');
    }

}
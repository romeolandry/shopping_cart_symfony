<?php


namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

use App\Entity\Product;
use App\Entity\Category;
use App\Form\ProductFormType;
use App\Form\CategoryFormType;

/**
 * @Route("/admin")
*/

class AdminController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var Environment
     */
    private $entityManager;

    /**
     * Default Constructor
     * @param Environment $twig
     * @param EntityManager $entityManager
     */
    public function __construct(Environment $twig, EntityManagerInterface $entityManager)
    {
        $this->twig= $twig;
        $this->entityManager = $entityManager;

    }


    /**
     * @Route("/", name="redirection")
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function redirectAdministration ()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirectToRoute('login');
            // throw $this->createAccessDeniedException();
        }
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return  $this->redirectToRoute('admin_panel'); # adminis
        }
        else
        {
            return  $this->redirectToRoute('marketplace');
        }
    }

    /**
     * @Route("/panel", name="admin_panel")
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function admin ()
    {

        return new Response($this->twig->render('Administration/admin.html.twig'));
    }

    /*
     * #################################################################################################################
     * ###################################### Product Administration ##################################################
     * #################################################################################################################
    */

    /**
     * @Route("/products", name="admin_products")
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function products ()
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();

        return new Response($this->twig->render('Administration/products.html.twig', [
            'products'=> $products
        ]));
    }

    /**
     * @Route("/products/disable/{id}", name="admin_product_update_set_inactive" , methods={"POST", "GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param Product $product
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function update_product_disable (Request $request, Product $product)
    {
        $this->entityManager->getRepository(Category::class);
        if($product->getAvailable() == 0){
            $product->setAvailable(1);
        }
        else{
            $product->setAvailable(0);
        }
        $this->entityManager->persist($product);
        $this->entityManager->flush();
        return $this->redirectToRoute('admin_products');
    }

    /**
     * @Route("/add/product/", name="admin_add_product" , methods={"POST", "GET"})
     * @param Request $request
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws \Exception
     */
    public  function add_product (Request $request)
    {
        $product = new Product();

        $productForm = $this->createForm(ProductFormType::class, $product);
        $productForm->handleRequest($request);

        if($productForm->isSubmitted() && $productForm->isValid())
        {
            $imageFile = $productForm->get('images')->getData();
            if($imageFile != null)
            {
                $fileName = str_replace(' ','_',$product->getName()).'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $this->getParameter('app.upload_dir_images'),
                    $fileName
                );
                $product->setImages(Product::FILE_PATH.$fileName);
            }
            else
            {
                $product->setImages(Product::DEFAULT_IMAGE);
            }
            $this->entityManager->getRepository(Category::class);
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            return  $this->redirectToRoute('admin_products');
        }

        return new Response($this->twig->render('Administration/ProductForm.html.twig',
            [
                'Product_form'=>$productForm->createView()
            ]));
    }

    /**
     * @Route("/products/update/{id}", name="admin_product_update" , methods={"POST", "GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param Product $product
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function update_product (Request $request, Product $product)
    {
        // Redirect to up

        // create instance of ProductForm to show form
        $productForm = $this->createForm(ProductFormType::class, $product);
        $productForm->handleRequest($request);

        if($productForm->isSubmitted() && $productForm->isValid())
        {
            $imageFile = $productForm->get('images')->getData();
            if($imageFile != null)
            {
                $fileName = str_replace(' ','_',$product->getName()).'.'.$imageFile->guessExtension();
                $imageFile->move(
                    $this->getParameter('app.upload_dir_images'),
                    $fileName
                );
                $product->setImages(Product::FILE_PATH.$fileName);
            }


            $this->entityManager->getRepository(Category::class);
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            return  $this->redirectToRoute('admin_products');
        }
        return new Response($this->twig->render('Administration/ProductForm.html.twig',
            [
                'Product_form'=>$productForm->createView()
            ]));
    }


    /*
     * #################################################################################################################
     * ###################################### Category Administration ##################################################
     * #################################################################################################################
    */

    /**
     * @Route("/categories", name="admin_categories")
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function categories_list ()
    {
        $list_categories = $this->entityManager->getRepository(Category::class)->findAll();
        dump($list_categories);

        return new Response($this->twig->render('Administration/categories.html.twig',
            ['categories' => $list_categories]));
    }

    /**
     * @Route("/categories/update/{id}", name="admin_categories_update" , methods={"POST", "GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param Category $category
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function update_category (Request $request, Category $category)
    {
        // Redirect to up

        // create instance of categoryFormType to show form
        $categoryForm = $this->createForm(CategoryFormType::class, $category);
        $categoryForm->handleRequest($request);

        if($categoryForm->isSubmitted() && $categoryForm->isValid())
        {

            $this->entityManager->getRepository(Category::class);
            $this->entityManager->persist($category);
            $this->entityManager->flush();
            return  $this->redirectToRoute('admin_categories');
        }
        return new Response($this->twig->render('Administration/CategoryForm.html.twig',
            [
                'category_form'=>$categoryForm->createView()
            ]));
    }

    /**
     * @Route("/categories/disable/{id}", name="admin_categories_update_set_inactive" , methods={"POST", "GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param Category $category
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function update_category_disable (Request $request, Category $category)
    {
        $this->entityManager->getRepository(Category::class);
        if($category->getAvailable() == 0){
            $category->setAvailable(1);
        }
        else{
            $category->setAvailable(0);
        }
        $this->entityManager->persist($category);
        $this->entityManager->flush();
        return $this->redirectToRoute('admin_categories');
    }

    /**
     * @Route("/add/category/", name="admin_add_category" , methods={"POST", "GET"})
     * @param Request $request
     * @param Category $category
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public  function add_category (Request $request)
    {
        $category = new Category();
        $categoryForm = $this->createForm(CategoryFormType::class, $category);
        $categoryForm->handleRequest($request);

        if($categoryForm->isSubmitted() && $categoryForm->isValid())
        {

            $this->entityManager->getRepository(Category::class);
            $this->entityManager->persist($category);
            $this->entityManager->flush();
            return  $this->redirectToRoute('admin_categories');
        }

        return new Response($this->twig->render('Administration/CategoryForm.html.twig',
            [
                'category_form'=>$categoryForm->createView()
            ]));
    }

}
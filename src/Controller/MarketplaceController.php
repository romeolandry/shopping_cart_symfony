<?php


namespace App\Controller;

use App\Entity\Product;
use App\Services\ServicesUsersCarCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

/**
 * @Route("/profile")
 */
class MarketplaceController extends AbstractController
{
    /**
     * @var Environment
    */
    private $twig;
    private $security;
    private $car_value;

    public function __construct(Environment $twig, ServicesUsersCarCommand $servicesUsersCarCommand, Security $security)
    {
        $this->twig = $twig;
        // get all open car for the connected user
        $user = $security->getUser();
        $this->car_value = $servicesUsersCarCommand->countOpenUserCar($user->getId());
    }

    /**
     * @Route("/marketplace", name="marketplace")
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public  function marketplaceRedirection ()
    {
        // get available product from DB
        $available = 1;
        $products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProduct($available);
        $row_count = ceil(count($products)/3);

        return new Response($this->twig->render('Marketplace/marketplace.html.twig',
            ['products'=>$products,
                'rowCount'=> $row_count,
                'car_value' =>$this->car_value
                ]));
    }

    /**
     * @Route("/marketplace/", name="marketplace_add_cart", methods={"GET"})
     * @param Request $request
     * @param Product $product
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public  function marketplace_add_to_car (Request $request, Product $product)
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProduct();
        dump("show all available product");
        dump($products);
        $row_count = ceil(count($products)/3);
        dump($row_count);

        return new Response($this->twig->render('Marketplace/marketplace.html.twig',['product'=>$product]));
    }



}
<?php


namespace App\Controller;


use App\Entity\Product;
use App\Services\ServicesUsersCarCommand;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

/**
 * @Route("/profile")
*/

class FilterController extends AbstractController
{
    /**
     * @var int
    */
    private $car_value;
    /**
     * @var array
     */
    private $openCar;
    /**
     * @var float
     */
    private $bil;

    public function __construct(Environment $twig, ServicesUsersCarCommand $servicesUsersCarCommand, Security $security)
    {
        $this->twig = $twig;
        // get all open car for the connected user
        $user = $security->getUser();
        $this->car_value = $servicesUsersCarCommand->countOpenUserCar($user->getId());
        $this->openCar = $servicesUsersCarCommand->getNotCommandedCar($user->getId());
        $this->bil = $servicesUsersCarCommand->bil($user->getId());
    }

    # reset page load data none Filter
    /**
     * @Route("/reset/{page}", name="reset_page", requirements={"page"="\S+"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function reset(Request $request)
    {
        if($request->get("page") == "cart")
        {
            // redirect to cart
            return $this->redirectToRoute('car');
        }
        else
        {
            // redirect to marketplace
            return $this->redirectToRoute('marketplace');
        }
    }

    # get filter values

    /**
     * @Route("/filter/", name="apply_filter", methods={"Get"})
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function apply(Request $request)
    {
        $isCar = true;

        $quantity = $request->get("quantity");
        $infos = $request->get("information");
        $priceFrom = $request->get("priceFrom");
        $priceTo = $request->get("priceTo");
        $filtered_products = [];
        $filtered_car = [];

        if (($quantity == null) or ($quantity == ""))
        {
            $isCar = false;
        }

        if ($isCar){

            if(($infos == "") and ($quantity == 0) and ($priceFrom == "") and ($priceTo == ""))
            {
                $this->addFlash(
                    'danger',
                    "set filter value !"
                );
                return $this->redirectToRoute('car');

            }
            else
            {
                if($quantity == 0 and $infos != "")
                {
                    $filtered_car = array_filter($this->openCar, function ($element) use ($infos) {
                        if((strpos($element->getProduct()->getName(),$infos)!==false) or (strpos($element->getProduct()->getDescription(),$infos)!==false))
                        {
                            return $element;
                        }

                    });

                }

                if($priceTo =="" and $priceFrom =="" and $quantity != 0 and $infos == "")
                {
                    $filtered_car = array_filter($this->openCar, function ($element) use ($quantity) {
                        if((strpos($element->getQuantity(),$quantity)!==false))
                        {
                            return $element;
                        }
                    });
                }
                if($priceTo =="" and $priceFrom =="" and $quantity != 0 and $infos != "")
                {
                    $filtered_car = array_filter($this->openCar, function ($element) use ($quantity, $infos) {
                        if(((strpos($element->getProduct()->getName(),$infos)!==false) or (strpos($element->getProduct()->getDescription(),$infos)!==false)) and (strpos($element->getQuantity(),$quantity)!==false))
                        {
                            return $element;
                        }
                    });
                }



            }
            if(count($filtered_car)!=0)
            {
                $this->addFlash(
                    'warning',
                    "the total amount not corresponding to total of filtered car "
                );
                return $this->render('car/index.html.twig', [
                    'open_car' => $filtered_car,
                    'car_value' => $this->car_value,
                    'bil' => $this->bil
                ]);

            }
            $this->addFlash(
                'info',
                "no corresponding entry for the given value!"
            );
            return $this->redirectToRoute('car');
        }
        else
        {
            if(($infos == "") and ($priceFrom == "") and ($priceTo == ""))
            {
                $this->addFlash(
                    'danger',
                    "set filter Values!"
                );
                return $this->redirectToRoute('marketplace');
            }
            else
            {
                if(($infos != "") or ($priceFrom != "") or ($priceTo != ""))
                {
                    if($priceTo =="" and $priceFrom =="" and $infos != "")
                    {
                        dump("only Info");
                        $filtered_products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProductFilterinformation($infos);
                        dump($filtered_products);
                        exit();
                    }

                    if($priceTo!="" or $priceFrom !="")
                    {

                        if($priceFrom > $priceTo)
                        {
                            $this->addFlash(
                                'danger',
                                "incorrect price range !"
                            );
                            return $this->redirectToRoute('marketplace');
                        }
                        else
                        {
                            $filtered_products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProductFilterPriceBetween($infos,$priceFrom, $priceTo);
                        }
                    }
                    else
                    {
                        if ($priceFrom !="")
                        {
                            $filtered_products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProductFilterpriceFrom($infos,$priceFrom);
                        }
                        if($priceTo !="")
                        {
                            $filtered_products = $this->getDoctrine()->getRepository(Product::class)->findAllAvailableProductFilterPriceto($infos,$priceTo);

                        }
                    }

                }
            }
            if(count($filtered_products)==0)
            {
                $this->addFlash(
                    'warning',
                    "there is not corresponding result !"
                );
                return $this->redirectToRoute('marketplace');
            }
            $row_count = ceil(count($filtered_products)/3);
            return new Response($this->twig->render('Marketplace/marketplace.html.twig',
                ['products'=>$filtered_products,
                    'rowCount'=> $row_count,
                    'car_value' =>$this->car_value
                ]));
        }

    }
}
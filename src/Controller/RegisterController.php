<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use App\Entity\User;

class RegisterController extends  AbstractController
{
    /**
     *  @param Environment  EventDispatcherInterface
    */
    public function __construct(UserPasswordEncoderInterface $password_encoder, EventDispatcherInterface $dispatcher)
    {
        $this->password_encoder = $password_encoder;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @Route("/login", name="login")
     *
     */
    public function login(AuthenticationUtils $helper)
    {

        $token = $this->get('security.token_storage')->getToken();
        $authEvent = new AuthenticationEvent($token);
        $this->dispatcher->dispatch($authEvent);

        return $this->render('register/FormLogin.html.twig', [
            'error'=>$helper->getLastAuthenticationError()
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout():void
    {
        throw new \Exception('this should never be reached');
    }

    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param Environment $twig
     * @param Environment event_dispatcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function register(Request $request)
    {
        if($request->get("_email")!== null){
            // check if an user with same email already exist
            $user_check = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($request->get('_email'));
            // ser_check = "test";
            if($user_check !== null){
                $this->addFlash(
                    'warning',
                    "User with same email already exist!"
                );
                return $this->render('register/FormRegister.html.twig');
            }
            else{
                $entitymangement = $this->getDoctrine()->getManager();
                $user = new User();
                $now = new \DateTime();
                $user->setName($request->get('_name'));
                $user->setEmail($request->get('_email'));
                $user->setPassword($this->password_encoder->encodePassword($user,$request->get('_password')));
                $user->setRoles(['ROLE_USER']);
                $user->setCreateOn($now);
                $user->setLastlogin($now);
                // save new user in to DB
                $entitymangement->persist($user);
                $entitymangement->flush();
                // programmatically login new user
                $token = new UsernamePasswordToken($user,$user->getPassword(),'main',$user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                return $this->redirectToRoute('homepage');

            }
        }
        else
        {
            return $this->render('register/FormRegister.html.twig');
        }


    }
}
<?php

namespace App\Controller;

use App\Entity\NoteEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class DefaultController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * DefaultController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return  $this->redirectToRoute('admin_panel'); # admin
        }
        elseif($this->get('security.authorization_checker')->isGranted('ROLE_USER') || $this->get('security.authorization_checker')->isGranted('ROLE_CUSTOMER_VISA'))
        {
            return  $this->redirectToRoute('marketplace');
        }
        else
        {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/test", name="test_route", methods={"POST", "GET"})
     *
     * @param Request $request
     * @param Environment $twig
     * @return Response
     *
     * @throws \Exception
     */
    public function test(Request $request): Response
    {
        $note = (new NoteEntity())
            ->setTitle('Das ist mein Titel')
            ->setDescription('Das ist eine Beschreibung.');

        return new Response($this->twig->render('test.html.twig'));
    }
}
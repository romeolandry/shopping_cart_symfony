<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class NoteEntity
 *
 * @ORM\Entity()
 * @ORM\Table(name="cart_notes")
 */
class NoteEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_changed", type="datetime", nullable=true)
     */
    private $lastChanged;

    /**
     * NoteEntity constructor.
     */
    public function __construct()
    {
        $this->created = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return NoteEntity
     */
    public function setTitle(string $title): NoteEntity
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return NoteEntity
     */
    public function setDescription(string $description): NoteEntity
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return \DateTime
     */
    public function getLastChanged(): \DateTime
    {
        return $this->lastChanged;
    }

    /**
     * @param \DateTime $lastChanged
     * @return NoteEntity
     */
    public function setLastChanged(\DateTime $lastChanged): NoteEntity
    {
        $this->lastChanged = $lastChanged;

        return $this;
    }
}
<?php

namespace App\Security\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RememberVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['CAR_VIEW'])
            && $subject instanceof \App\Entity\car;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $car = $subject;
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        switch ($attribute) {
            case 'CAR_VIEW':
                // logic to determine if the user can view car
                // if user role is  ROLE_CUSTOMER_VISA  and status is REMEMBERED IS_AUTHENTICATED_REMEMBERED
                // then return: the user should login !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')
                if(($this->security->isGranted("ROLE_CUSTOMER_VISA")) && ($car->getUser()->getId() == $user->getId()))
                {
                    return !$this->security->isGranted('IS_AUTHENTICATED_FULLY') && $this->security->isGranted('IS_AUTHENTICATED_REMEMBERED');
                }
                break;
        }

        return false;
    }
}

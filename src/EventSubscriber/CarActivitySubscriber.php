<?php

namespace App\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CarActivitySubscriber implements EventSubscriberInterface
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onUpdate($event)
    {
        // ...
    }

    public static function getSubscribedEvents()
    {
        return [
            'EventSubscriber' => 'onUpdate',
        ];
    }
}

<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
    * @return Product[] Returns an array of Product objects
    */
    // set eager loading to get all available product with corresponding category
    public function findAllAvailableProduct($value)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.category','c')
            ->addSelect('c')
            ->andWhere('p.available = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $information
     * @param $priceFrom
     * @param $priceTo
     * @return Product[] Returns an array of Product objects
     */
    // set eager loading to get all available product with corresponding category
    public function findAllAvailableProductFilterPriceBetween($information, $priceFrom, $priceTo)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.category','c')
            ->addSelect('c')
            ->andWhere('p.name LIKE  :val1')
            ->orWhere('p.description LIKE :val1')
            ->andWhere('p.price BETWEEN :val2 AND :val3')
            ->andWhere('p.available = :val')
            ->setParameter('val', 1)
            ->setParameter('val1', '%'.$information.'%')
            ->setParameter('val2', $priceFrom)
            ->setParameter('val3', $priceTo)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $information
     * @return Product[] Returns an array of Product objects
     */
    public function findAllAvailableProductFilterinformation($information)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.category','c')
            ->addSelect('c')
            ->andWhere('p.name LIKE  :val1')
            ->orWhere('p.description LIKE :val1')
            ->andWhere('p.available = :val')
            ->setParameter('val', 1)
            ->setParameter('val1', '%'.$information.'%')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $information
     * @param $priceFrom
     *  @return Product[] Returns an array of Product objects
     */
    public function findAllAvailableProductFilterpriceFrom($information, $priceFrom)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.category','c')
            ->addSelect('c')
            ->andWhere('p.name LIKE  :val1')
            ->orWhere('p.description LIKE :val1')
            ->andWhere('p.price >= :val2')
            ->andWhere('p.available = :val')
            ->setParameter('val', 1)
            ->setParameter('val1', '%'.$information.'%')
            ->setParameter('val2', $priceFrom)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $information
     * @param $priceTo
     * @return Product[] Returns an array of Product objects
     */
    public function findAllAvailableProductFilterPriceto($information, $priceTo)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.category','c')
            ->addSelect('c')
            ->andWhere('p.name LIKE  :val1')
            ->orWhere('p.description LIKE :val1')
            ->andWhere('p.price <= :val2')
            ->andWhere('p.available = :val')
            ->setParameter('val', 1)
            ->setParameter('val1', '%'.$information.'%')
            ->setParameter('val2', $priceTo)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

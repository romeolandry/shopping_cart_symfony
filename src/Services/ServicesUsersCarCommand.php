<?php


namespace App\Services;


use App\Entity\Car;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use ErrorException;

class ServicesUsersCarCommand
{
 // this service will set  user all opened car for user
    private $notCommandedCar = [];
    private $allUserCar;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAllUserCar($user_id)
    {
        return $this->entityManager->getRepository(User::class)->findUserAndCar($user_id);
    }

    public function getNotCommandedCar($user_id)
    {
        $this->notCommandedCar = [];
        $this->allUserCar = $this->entityManager->getRepository(User::class)->findUserAndCar($user_id);

        if ($this->allUserCar == NULL){
            return $this->notCommandedCar;
        }
        else
        {
            foreach ($this->allUserCar->getCar() as $car)
            {
                $car_command = $this->entityManager->getRepository(Car::class)->checkIfCartCommanded($car->getId());
                if ($car_command == null)
                {
                    array_push($this->notCommandedCar, $car);
                }
            }
        }
        return $this->notCommandedCar;
    }

    public function countOpenUserCar($user_id)
    {
      return count($this->getNotCommandedCar($user_id));
    }

    public function bil($user_id)
    {
        $car_notCommand = $this->getNotCommandedCar($user_id);
        $total_Amount = 0;
        foreach ($car_notCommand as $item)
        {
            $total_Amount = $total_Amount + $item->getPrice();
        }
        return $total_Amount;
    }

}